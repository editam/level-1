'''
#2
Question:
Write a program which can compute the factorial of a given number.
Suppose the following input is supplied to the program:
8
Then, the output should be:
40320
'''
def factorial_with_recursion(num):
   if num <1:   # base case
       return 1
   else:
       returnNumber = num * factorial_with_recursion( num - 1 )
       return returnNumber

def factorial(inp):
    outp = 1
    for i in range (inp, 0, -1):
        outp *= i
    return outp

inp = int(raw_input('Enter a number: '))
print inp,"! =", fact_with_rec(inp)


