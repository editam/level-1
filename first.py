'''
#1
Question:
Write a program which will find all such numbers which are divisible by 7 but are not divisible by 5,
between 2000 and 3200 (inclusive).
The numbers obtained should be printed in a comma-separated sequence on a single line.
'''

number_list = [str(x) for x in range(2000, 3200) if x % 7 == 0 and x % 5 != 0]

print ",".join(number_list)
