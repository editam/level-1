'''
#3
Question:
Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple which contains every number.
Suppose the following input is supplied to the program:
34,67,55,33,12,98
Then, the output should be:
['34', '67', '55', '33', '12', '98']
('34', '67', '55', '33', '12', '98')
'''

def numbers_to_list(string):
    return list(int(x) for x in string.split(','))

def numbers_to_tuple(string):
    return tuple(int(x) for x in string.split(','))

inp = raw_input('Enter line of numbers: ')
print numbers_to_list(inp)
print numbers_to_tuple(inp)
