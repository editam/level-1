def square(n):
    return n ** 2

my_map = map(square, [n for n in range(1, 21)])
print my_map
