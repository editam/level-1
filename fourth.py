'''
#4
Question:
Define a class which has at least two methods:
getString: to get a string from console input
printString: to print the string in upper case.
'''

class myClass(object):
    def __init__(self):
        string = ""
    
    def getString(self):
        self.string = raw_input("Enter something: ")
        return self.string
    
    def printString(self):
        print self.string.upper()

my_obj = myClass()
string = my_obj.getString()
my_obj.printString()
