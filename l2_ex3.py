def remove_duplicates(inp):
    return set(inp)

def sort(*kwargs):
    if len(kwargs) == 1 or kwargs[1] == False:
        return sorted(kwargs[0])
    elif kwargs[1] == True:
        return sorted(kwargs[0], reverse = True)
    
inp = raw_input("Enter text: ")
inp_set = remove_duplicates(inp.split(" "))

print " ".join(sort(inp_set))
print " ".join(sort(inp_set, True))
