def create_array(x, y):
    array = [[] for _ in range(x)]
    for i in range(x):
        for j in range(y):
            array[i].append(i*j)
    return array

x,y = raw_input('Enter x,y: ').strip().split(",")

x = int(x)
y = int(y)
print create_array(x,y)
