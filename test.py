'''
BONUS: write tests for class methods. You can run them using nosetests (tutorial: http://pythontesting.net/framework/nose/nose-introduction/)
INSTALL: sudo -H pip install nose
'''

from fourth import myClass

def test_getString():
    obj = myClass()
    assert obj.getString() != None

def test_printString():
    obj = myClass()
    string = obj.getString()
    printed = False
    try:
        obj.printString()
        printed = True
    except:
        printed = False
        
    assert printed == True

